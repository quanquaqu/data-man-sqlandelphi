CREATE TABLE T1
(	卫星ID varchar(10) not null,
	型号 varchar(50) ,   
	使用时长 int check(使用时长 >= 0 and 使用时长 <= 100000),
	所属机构 varchar(50),
	primary key(卫星ID),
	foreign key (型号) references T2 (型号)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
);