create table T3
(  
卫星ID varchar(10) not null,
时间 varchar(20) not null,
对应地面站 varchar(10),
primary key(卫星ID,时间),
foreign key (卫星ID) references T1 (卫星ID)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
)  
create table T4
(  
卫星ID varchar(10) not null,
时间 varchar(20) not null,
轨道高度 varchar(50) ,   
轨道角度 varchar(50) ,   
卫星能量 varchar(50) ,
primary key(卫星ID,时间),
foreign key (卫星ID) references T1 (卫星ID)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
)  
create table T5
(  
卫星ID varchar(10) not null,
操纵时间 varchar(50)not null ,
操纵人员 varchar(50) ,   
primary key(卫星ID,操纵时间),
foreign key (卫星ID) references T1 (卫星ID)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
)  
create table T6
(  
卫星ID varchar(10) not null,
任务编号 varchar(50) not null,  
执行时间 varchar(50) not null,
结果 varchar(50) ,   
任务性质 varchar(50)
primary key(卫星ID,任务编号,执行时间),
foreign key (卫星ID) references T1 (卫星ID)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
)  
