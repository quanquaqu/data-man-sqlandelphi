unit Unit8;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids;
type
  TForm8 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    DBGrid1: TDBGrid;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation
uses unit3;
{$R *.dfm}

procedure TForm8.Button1Click(Sender: TObject);
begin
form3.ADOQuery5.Close;
form3.ADOQuery5.SQL.Clear;
form3.ADOQuery5.SQL.Text:='insert into T5 values(:a,:b,:c)';
form3.ADOQuery5.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery5.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery5.Parameters.ParamByName('c').Value:=Edit3.Text;
form3.ADOQuery5.ExecSQL;
form3.ADOQuery5.Close;
form3.ADOQuery5.SQL.Clear;
form3.ADOQuery5.SQL.Add('select * from T5');
form3.ADOQuery5.Open;
end;

procedure TForm8.Button2Click(Sender: TObject);
begin
form3.ADOQuery3.Close;
form3.ADOQuery5.SQL.Clear;
form3.ADOQuery5.SQL.Text:='delete from T5 where 卫星ID=:a and 操纵时间=:b';
form3.ADOQuery5.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery5.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery5.ExecSQL;
form3.ADOQuery5.Close;
form3.ADOQuery5.SQL.Clear;
form3.ADOQuery5.SQL.Add('select * from T5');
form3.ADOQuery5.Open;
end;

procedure TForm8.Button3Click(Sender: TObject);
begin
form3.ADOQuery5.Close;
form3.ADOQuery5.SQL.Clear;
form3.ADOQuery5.SQL.Text:='update T5 set 卫星ID=:c,操纵时间=:d,操纵人员=:e where 卫星ID=:a and 操纵时间=:b';
form3.ADOQuery5.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery5.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery5.Parameters.ParamByName('c').Value:=Edit4.Text;
form3.ADOQuery5.Parameters.ParamByName('d').Value:=Edit5.Text;
form3.ADOQuery5.Parameters.ParamByName('e').Value:=Edit6.Text;
form3.ADOQuery5.ExecSQL;
form3.ADOQuery5.Close;
form3.ADOQuery5.SQL.Clear;
form3.ADOQuery5.SQL.Add('select * from T5');
form3.ADOQuery5.Open;
end;

procedure TForm8.Button4Click(Sender: TObject);
begin
form3.ADOQuery5.Close;
form3.ADOQuery5.SQL.Clear;
form3.ADOQuery5.SQL.Add('select * from T5 where 卫星ID=:a and 操纵时间=:b');
form3.ADOQuery5.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery5.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery5.Open;
end;

procedure TForm8.Button5Click(Sender: TObject);
begin
form3.ADOQuery5.Close;
form3.ADOQuery5.SQL.Clear;
form3.ADOQuery5.SQL.Add('select * from T5');
form3.ADOQuery5.Open;
end;

end.