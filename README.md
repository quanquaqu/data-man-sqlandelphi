# Data management app based on SQL server and Delphi.（数据管理软件）

#### 介绍
This is a data-management app implementation for my <Database design and manage> course at school, which supports insert/delete/query. It's based on SQL server and Delphi.

#### 软件架构

代码文件夹中包含本项目的SQL源代码以及Delphi源代码

设计文档中包含设计文档及相关图示

使用说明中包含使用说明书，相关图示，视频讲解

#### 使用说明

可执行文件在文件夹 软件及数据库备份 ->Projects中，其余的项目相关文件也在此文件夹中，可供您在Delphi中进行调试。

关于数据，这里只提供了T1和T2中插入数据的SQL代码，其余数据您可以自行添加，在数据库Quan中也有着适量的数据可供您进行测试。

基于条件的限制，本软件仅仅在四台电脑上进行过测试，配置成功后软件都可以正常运行，增删改查的功能也没有出现问题。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


附：

开发者的一些话：
  虽然本软件的开发时间比不上某些大型软件动辄好几个月甚至好几年的长周期，也没有组内的同事互相进行code review，但是也确实倾注了本人的心血。
  希望本软件的使用者和测试者不因为一些短暂的报错就丧失对本软件的信心，请联系技术支持，我会积极为您解决问题。

