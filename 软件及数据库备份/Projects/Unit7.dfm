object Form7: TForm7
  Left = 0
  Top = 0
  Caption = 'Form7'
  ClientHeight = 521
  ClientWidth = 884
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 362
    Top = 8
    Width = 168
    Height = 25
    Caption = #31649#29702#36712#36947#30456#20851#20449#24687
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 30
    Top = 50
    Width = 49
    Height = 19
    Caption = #21355#26143'ID'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 30
    Top = 80
    Width = 32
    Height = 19
    Caption = #26102#38388
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 30
    Top = 110
    Width = 64
    Height = 19
    Caption = #36712#36947#39640#24230
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 30
    Top = 140
    Width = 64
    Height = 19
    Caption = #36712#36947#35282#24230
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 640
    Top = 20
    Width = 108
    Height = 13
    Caption = #22686#38656#35201#36755#20837#20840#37096#25968#25454
  end
  object Label7: TLabel
    Left = 640
    Top = 40
    Width = 143
    Height = 13
    Caption = #21024#21482#38656#35201#36755#20837#21355#26143'ID'#21644#26102#38388
  end
  object Label9: TLabel
    Left = 640
    Top = 100
    Width = 143
    Height = 13
    Caption = #26597#21482#38656#35201#36755#20837#21355#26143'ID'#21644#26102#38388
  end
  object Label10: TLabel
    Left = 640
    Top = 60
    Width = 179
    Height = 13
    Caption = #22914#25913#65292#35831#22312#24038#20391#36755#20837#21355#26143'ID'#21644#26102#38388
  end
  object Label8: TLabel
    Left = 640
    Top = 80
    Width = 168
    Height = 13
    Caption = #24182#22312#19979#26041#36755#20837#26032#25968#25454#30340#20840#37096#20449#24687
  end
  object Label11: TLabel
    Left = 30
    Top = 170
    Width = 64
    Height = 19
    Caption = #21355#26143#33021#37327
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 150
    Top = 53
    Width = 100
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 150
    Top = 80
    Width = 100
    Height = 21
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 150
    Top = 113
    Width = 100
    Height = 21
    TabOrder = 2
  end
  object Edit4: TEdit
    Left = 150
    Top = 140
    Width = 100
    Height = 21
    TabOrder = 3
  end
  object Button1: TButton
    Left = 362
    Top = 95
    Width = 75
    Height = 25
    Caption = #22686
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 480
    Top = 95
    Width = 75
    Height = 25
    Caption = #21024
    TabOrder = 5
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 362
    Top = 160
    Width = 75
    Height = 25
    Caption = #25913
    TabOrder = 6
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 480
    Top = 160
    Width = 75
    Height = 25
    Caption = #26597
    TabOrder = 7
    OnClick = Button4Click
  end
  object Edit5: TEdit
    Left = 150
    Top = 170
    Width = 100
    Height = 21
    TabOrder = 8
  end
  object Edit6: TEdit
    Left = 256
    Top = 53
    Width = 100
    Height = 21
    TabOrder = 9
  end
  object Edit7: TEdit
    Left = 256
    Top = 80
    Width = 100
    Height = 21
    TabOrder = 10
  end
  object Edit8: TEdit
    Left = 256
    Top = 113
    Width = 100
    Height = 21
    TabOrder = 11
  end
  object Edit9: TEdit
    Left = 256
    Top = 140
    Width = 100
    Height = 21
    TabOrder = 12
  end
  object Edit10: TEdit
    Left = 256
    Top = 170
    Width = 100
    Height = 21
    TabOrder = 13
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 250
    Width = 900
    Height = 250
    DataSource = Form3.DataSource4
    TabOrder = 14
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button5: TButton
    Left = 647
    Top = 160
    Width = 100
    Height = 25
    Caption = #26597#35810#20840#37096#25968#25454
    TabOrder = 15
    OnClick = Button5Click
  end
end
