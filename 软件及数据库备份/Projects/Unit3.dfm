object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 521
  ClientWidth = 864
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 168
    Top = 72
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from T1')
    Left = 24
    Top = 72
  end
  object ADOQuery2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from T2')
    Left = 96
    Top = 72
  end
  object ADOQuery3: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from T3')
    Left = 24
    Top = 136
  end
  object ADOQuery4: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from T4')
    Left = 96
    Top = 136
  end
  object ADOQuery5: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from T5')
    Left = 24
    Top = 192
  end
  object ADOQuery6: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from T6')
    Left = 96
    Top = 192
  end
  object DataSource2: TDataSource
    DataSet = ADOQuery2
    Left = 240
    Top = 72
  end
  object DataSource3: TDataSource
    DataSet = ADOQuery3
    Left = 168
    Top = 136
  end
  object DataSource4: TDataSource
    DataSet = ADOQuery4
    Left = 240
    Top = 136
  end
  object DataSource5: TDataSource
    DataSet = ADOQuery5
    Left = 168
    Top = 192
  end
  object DataSource6: TDataSource
    DataSet = ADOQuery6
    Left = 240
    Top = 192
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=Quan_man' +
      ';Initial Catalog=Quan'
    Provider = 'SQLOLEDB.1'
    Left = 32
    Top = 24
  end
end
