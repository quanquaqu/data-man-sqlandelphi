unit Unit5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls;

type
  TForm5 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Edit5: TEdit;
    Label4: TLabel;
    Edit3: TEdit;
    Edit4: TEdit;
    DBGrid1: TDBGrid;
    Button5: TButton;
    Label5: TLabel;
    Label11: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation
uses unit3;
{$R *.dfm}

procedure TForm5.Button1Click(Sender: TObject);
begin
form3.ADOQuery2.Close;
form3.ADOQuery2.SQL.Clear;
form3.ADOQuery2.SQL.Text:='insert into T2 values(:a,:b,:c)';
form3.ADOQuery2.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery2.Parameters.ParamByName('b').Value:=StrToInt(Edit2.Text);
form3.ADOQuery2.Parameters.ParamByName('c').Value:=StrToInt(Edit3.Text);
form3.ADOQuery2.ExecSQL;
form3.ADOQuery2.Close;
form3.ADOQuery2.SQL.Clear;
form3.ADOQuery2.SQL.Add('select * from T2');
form3.ADOQuery2.Open;
end;

procedure TForm5.Button2Click(Sender: TObject);
begin
form3.ADOQuery2.Close;
form3.ADOQuery2.SQL.Clear;
form3.ADOQuery2.SQL.Text:='delete from T2 where �ͺ�=:a';
form3.ADOQuery2.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery2.ExecSQL;
form3.ADOQuery2.Close;
form3.ADOQuery2.SQL.Clear;
form3.ADOQuery2.SQL.Add('select * from T2');
form3.ADOQuery2.Open;
end;

procedure TForm5.Button3Click(Sender: TObject);
begin
form3.ADOQuery2.Close;
form3.ADOQuery2.SQL.Clear;
form3.ADOQuery2.SQL.Text:='exec update_t2 :a,:b,:c';
form3.ADOQuery2.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery2.Parameters.ParamByName('b').Value:=Edit4.Text;
form3.ADOQuery2.Parameters.ParamByName('c').Value:=StrToInt(Edit5.Text);
form3.ADOQuery2.ExecSQL;
form3.ADOQuery2.Close;
form3.ADOQuery2.SQL.Clear;
form3.ADOQuery2.SQL.Add('select * from T2');
form3.ADOQuery2.Open;
end;

procedure TForm5.Button4Click(Sender: TObject);
begin
form3.ADOQuery2.Close;
form3.ADOQuery2.SQL.Clear;
form3.ADOQuery2.SQL.Add('select * from T2 where �ͺ�=:a');
form3.ADOQuery2.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery2.Open;
end;

procedure TForm5.Button5Click(Sender: TObject);
begin
form3.ADOQuery2.Close;
form3.ADOQuery2.SQL.Clear;
form3.ADOQuery2.SQL.Add('select * from T2');
form3.ADOQuery2.Open;
end;

end.
