object Form5: TForm5
  Left = 0
  Top = 0
  Caption = 'Form5'
  ClientHeight = 521
  ClientWidth = 884
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 362
    Top = 8
    Width = 168
    Height = 25
    Caption = #31649#29702#22411#21495#30456#20851#20449#24687
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 30
    Top = 50
    Width = 32
    Height = 19
    Caption = #22411#21495
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 30
    Top = 100
    Width = 80
    Height = 19
    Caption = #21487#20351#29992#26102#38271
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 640
    Top = 20
    Width = 108
    Height = 13
    Caption = #22686#38656#35201#36755#20837#20840#37096#25968#25454
  end
  object Label7: TLabel
    Left = 641
    Top = 39
    Width = 96
    Height = 13
    Caption = #21024#21482#38656#35201#36755#20837#22411#21495
  end
  object Label9: TLabel
    Left = 640
    Top = 99
    Width = 96
    Height = 13
    Caption = #26597#21482#38656#35201#36755#20837#22411#21495
  end
  object Label10: TLabel
    Left = 640
    Top = 61
    Width = 132
    Height = 13
    Caption = #22914#25913#65292#35831#22312#24038#20391#36755#20837#22411#21495
  end
  object Label8: TLabel
    Left = 641
    Top = 80
    Width = 168
    Height = 13
    Caption = #24182#22312#19979#26041#36755#20837#26032#25968#25454#30340#20840#37096#20449#24687
  end
  object Label4: TLabel
    Left = 30
    Top = 148
    Width = 48
    Height = 19
    Caption = #21355#26143#25968
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 16
    Top = 201
    Width = 690
    Height = 16
    Caption = #24744#19981#33021#30452#25509#26356#25913#21355#26143#25968#65292#36825#26159#22240#20026#24744#39318#20808#38656#35201#22686#21152#25110#21024#38500#21355#26143#20449#24687#65292#27492#26102#23545#24212#22411#21495#30340#21355#26143#25968#20250#33258#21160#26356#25913#12290
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 8
    Top = 223
    Width = 810
    Height = 16
    Caption = #65288#27492#22788#24182#38750#35774#35745#22833#35823#65292#32780#26159#26377#24847#20026#20043#65292#36825#26159#22240#20026#21355#26143#25968#30446#20869#30340#27599#19968#20010#21355#26143#37117#26159#24517#39035#35201#26377#23454#38469#30340#21355#26143#23454#20307#19982#20854#23545#24212#30340#65281#65281#65281#65289
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 150
    Top = 50
    Width = 100
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 150
    Top = 100
    Width = 100
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 362
    Top = 100
    Width = 75
    Height = 25
    Caption = #22686
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 480
    Top = 98
    Width = 75
    Height = 25
    Caption = #21024
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 362
    Top = 160
    Width = 75
    Height = 25
    Caption = #25913
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 480
    Top = 160
    Width = 75
    Height = 25
    Caption = #26597
    TabOrder = 5
    OnClick = Button4Click
  end
  object Edit5: TEdit
    Left = 256
    Top = 100
    Width = 100
    Height = 21
    TabOrder = 6
  end
  object Edit3: TEdit
    Left = 150
    Top = 148
    Width = 100
    Height = 21
    TabOrder = 7
  end
  object Edit4: TEdit
    Left = 256
    Top = 50
    Width = 100
    Height = 21
    TabOrder = 8
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 250
    Width = 700
    Height = 241
    DataSource = Form3.DataSource2
    TabOrder = 9
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button5: TButton
    Left = 637
    Top = 160
    Width = 100
    Height = 25
    Caption = #26597#35810#20840#37096#25968#25454
    TabOrder = 10
    OnClick = Button5Click
  end
end
