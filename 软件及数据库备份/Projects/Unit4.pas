unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls;

type
  TForm4 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    DBGrid1: TDBGrid;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Label10: TLabel;
    Label8: TLabel;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation
uses unit3;
{$R *.dfm}

procedure TForm4.Button1Click(Sender: TObject);
begin
form3.ADOQuery1.Close;
form3.ADOQuery1.SQL.Clear;
form3.ADOQuery1.SQL.Text:='insert into T1 values(:a,:b,:c,:d)';
form3.ADOQuery1.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery1.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery1.Parameters.ParamByName('c').Value:=StrToInt(Edit3.Text);
form3.ADOQuery1.Parameters.ParamByName('d').Value:=Edit4.Text;
form3.ADOQuery1.ExecSQL;
form3.ADOQuery1.Close;
form3.ADOQuery1.SQL.Clear;
form3.ADOQuery1.SQL.Add('select * from T1');
form3.ADOQuery1.Open;
end;

procedure TForm4.Button2Click(Sender: TObject);
begin
form3.ADOQuery1.Close;
form3.ADOQuery1.SQL.Clear;
form3.ADOQuery1.SQL.Text:='delete from T1 where ����ID=:a';
form3.ADOQuery1.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery1.ExecSQL;
form3.ADOQuery1.Close;
form3.ADOQuery1.SQL.Clear;
form3.ADOQuery1.SQL.Add('select * from T1');
form3.ADOQuery1.Open;
end;

procedure TForm4.Button3Click(Sender: TObject);
begin
form3.ADOQuery1.Close;
form3.ADOQuery1.SQL.Clear;
form3.ADOQuery1.SQL.Text:='update t1 set ����ID=:b,�ͺ�=:c,ʹ��ʱ��=:d,��������=:e where ����ID=:a';
form3.ADOQuery1.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery1.Parameters.ParamByName('b').Value:=Edit5.Text;
form3.ADOQuery1.Parameters.ParamByName('c').Value:=Edit6.Text;
form3.ADOQuery1.Parameters.ParamByName('d').Value:=StrToInt(Edit7.Text);
form3.ADOQuery1.Parameters.ParamByName('e').Value:=Edit8.Text;
form3.ADOQuery1.ExecSQL;
form3.ADOQuery1.Close;
form3.ADOQuery1.SQL.Clear;
form3.ADOQuery1.SQL.Add('select * from T1');
form3.ADOQuery1.Open;
end;


procedure TForm4.Button4Click(Sender: TObject);
begin
form3.ADOQuery1.Close;
form3.ADOQuery1.SQL.Clear;
form3.ADOQuery1.SQL.Add('select * from T1 where ����ID=:a');
form3.ADOQuery1.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery1.Open;
end;

procedure TForm4.Button5Click(Sender: TObject);
begin
form3.ADOQuery1.Close;
form3.ADOQuery1.SQL.Clear;
form3.ADOQuery1.SQL.Add('select * from T1');
form3.ADOQuery1.Open;
end;

end.
