unit Unit9;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids;
type
  TForm9 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    DBGrid1: TDBGrid;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form9: TForm9;

implementation
uses unit3;
{$R *.dfm}

procedure TForm9.Button1Click(Sender: TObject);
begin
form3.ADOQuery6.Close;
form3.ADOQuery6.SQL.Clear;
form3.ADOQuery6.SQL.Text:='insert into T6 values(:a,:b,:c,:d,:e)';
form3.ADOQuery6.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery6.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery6.Parameters.ParamByName('c').Value:=Edit3.Text;
form3.ADOQuery6.Parameters.ParamByName('d').Value:=Edit4.Text;
form3.ADOQuery6.Parameters.ParamByName('e').Value:=Edit5.Text;
form3.ADOQuery6.ExecSQL;
form3.ADOQuery6.Close;
form3.ADOQuery6.SQL.Clear;
form3.ADOQuery6.SQL.Add('select * from T6');
form3.ADOQuery6.Open;
end;

procedure TForm9.Button2Click(Sender: TObject);
begin
form3.ADOQuery6.Close;
form3.ADOQuery6.SQL.Clear;
form3.ADOQuery6.SQL.Text:='delete from T4 where 卫星ID=:a and 任务编号=:b and 执行时间=:c';
form3.ADOQuery6.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery6.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery6.Parameters.ParamByName('c').Value:=Edit3.Text;
form3.ADOQuery6.ExecSQL;
form3.ADOQuery6.Close;
form3.ADOQuery6.SQL.Clear;
form3.ADOQuery6.SQL.Add('select * from T6');
form3.ADOQuery6.Open;
end;

procedure TForm9.Button3Click(Sender: TObject);
begin
form3.ADOQuery6.Close;
form3.ADOQuery6.SQL.Clear;
form3.ADOQuery6.SQL.Text:='update T4 set 卫星ID=:d,时间=:e,轨道高度=:f,轨道角度=:g, 卫星能量=:h where 卫星ID=:a and 任务编号=:b and 执行时间=:c';
form3.ADOQuery6.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery6.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery6.Parameters.ParamByName('c').Value:=Edit3.Text;
form3.ADOQuery6.Parameters.ParamByName('d').Value:=Edit6.Text;
form3.ADOQuery6.Parameters.ParamByName('e').Value:=Edit7.Text;
form3.ADOQuery6.Parameters.ParamByName('f').Value:=Edit8.Text;
form3.ADOQuery6.Parameters.ParamByName('g').Value:=Edit9.Text;
form3.ADOQuery6.Parameters.ParamByName('h').Value:=Edit10.Text;
form3.ADOQuery6.ExecSQL;
form3.ADOQuery6.Close;
form3.ADOQuery6.SQL.Clear;
form3.ADOQuery6.SQL.Add('select * from T6');
form3.ADOQuery6.Open;
end;

procedure TForm9.Button4Click(Sender: TObject);
begin
form3.ADOQuery6.Close;
form3.ADOQuery6.SQL.Clear;
form3.ADOQuery6.SQL.Add('select * from T4 where 卫星ID=:a and 任务编号=:b and 执行时间=:c');
form3.ADOQuery6.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery6.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery6.Parameters.ParamByName('c').Value:=Edit3.Text;
form3.ADOQuery6.Open;
end;

procedure TForm9.Button5Click(Sender: TObject);
begin
form3.ADOQuery6.Close;
form3.ADOQuery6.SQL.Clear;
form3.ADOQuery6.SQL.Add('select * from T6');
form3.ADOQuery6.Open;
end;

end.
