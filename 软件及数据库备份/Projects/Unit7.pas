unit Unit7;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids;

type
  TForm7 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Label11: TLabel;
    Edit9: TEdit;
    Edit10: TEdit;
    DBGrid1: TDBGrid;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation
uses unit3;
{$R *.dfm}

procedure TForm7.Button1Click(Sender: TObject);
begin
form3.ADOQuery4.Close;
form3.ADOQuery4.SQL.Clear;
form3.ADOQuery4.SQL.Text:='insert into T4 values(:a,:b,:c,:d,:e)';
form3.ADOQuery4.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery4.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery4.Parameters.ParamByName('c').Value:=Edit3.Text;
form3.ADOQuery4.Parameters.ParamByName('d').Value:=Edit4.Text;
form3.ADOQuery4.Parameters.ParamByName('e').Value:=Edit5.Text;
form3.ADOQuery4.ExecSQL;
form3.ADOQuery4.Close;
form3.ADOQuery4.SQL.Clear;
form3.ADOQuery4.SQL.Add('select * from T4');
form3.ADOQuery4.Open;
end;

procedure TForm7.Button2Click(Sender: TObject);
begin
form3.ADOQuery4.Close;
form3.ADOQuery4.SQL.Clear;
form3.ADOQuery4.SQL.Text:='delete from T4 where 卫星ID=:a and 时间=:b';
form3.ADOQuery4.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery4.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery4.ExecSQL;
form3.ADOQuery4.Close;
form3.ADOQuery4.SQL.Clear;
form3.ADOQuery4.SQL.Add('select * from T4');
form3.ADOQuery4.Open;
end;

procedure TForm7.Button3Click(Sender: TObject);
begin
form3.ADOQuery4.Close;
form3.ADOQuery4.SQL.Clear;
form3.ADOQuery4.SQL.Text:='update T4 set 卫星ID=:c,时间=:d,轨道高度=:e,轨道角度=:f, 卫星能量=:g where 卫星ID=:a and 时间=:b';
form3.ADOQuery4.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery4.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery4.Parameters.ParamByName('c').Value:=Edit6.Text;
form3.ADOQuery4.Parameters.ParamByName('d').Value:=Edit7.Text;
form3.ADOQuery4.Parameters.ParamByName('e').Value:=Edit8.Text;
form3.ADOQuery4.Parameters.ParamByName('f').Value:=Edit9.Text;
form3.ADOQuery4.Parameters.ParamByName('g').Value:=Edit10.Text;
form3.ADOQuery4.ExecSQL;
form3.ADOQuery4.Close;
form3.ADOQuery4.SQL.Clear;
form3.ADOQuery4.SQL.Add('select * from T4');
form3.ADOQuery4.Open;
end;

procedure TForm7.Button4Click(Sender: TObject);
begin
form3.ADOQuery4.Close;
form3.ADOQuery4.SQL.Clear;
form3.ADOQuery4.SQL.Add('select * from T4 where 卫星ID=:a and 时间=:b');
form3.ADOQuery4.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery4.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery4.Open;
end;

procedure TForm7.Button5Click(Sender: TObject);
begin
form3.ADOQuery4.Close;
form3.ADOQuery4.SQL.Clear;
form3.ADOQuery4.SQL.Add('select * from T4');
form3.ADOQuery4.Open;
end;

end.
