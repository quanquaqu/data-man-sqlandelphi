object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Form4'
  ClientHeight = 521
  ClientWidth = 884
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 365
    Top = 8
    Width = 168
    Height = 25
    Caption = #31649#29702#21355#26143#30456#20851#20449#24687
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 30
    Top = 50
    Width = 49
    Height = 19
    Caption = #21355#26143'ID'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 30
    Top = 100
    Width = 32
    Height = 19
    Caption = #22411#21495
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 30
    Top = 150
    Width = 64
    Height = 19
    Caption = #20351#29992#26102#38271
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 30
    Top = 200
    Width = 64
    Height = 19
    Caption = #25152#23646#26426#26500
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 640
    Top = 20
    Width = 108
    Height = 13
    Caption = #22686#38656#35201#36755#20837#20840#37096#25968#25454
  end
  object Label7: TLabel
    Left = 640
    Top = 40
    Width = 107
    Height = 13
    Caption = #21024#21482#38656#35201#36755#20837#21355#26143'ID'
  end
  object Label9: TLabel
    Left = 640
    Top = 100
    Width = 107
    Height = 13
    Caption = #26597#21482#38656#35201#36755#20837#21355#26143'ID'
  end
  object Label10: TLabel
    Left = 640
    Top = 61
    Width = 143
    Height = 13
    Caption = #22914#25913#65292#35831#22312#24038#20391#36755#20837#21355#26143'ID'
  end
  object Label8: TLabel
    Left = 640
    Top = 80
    Width = 168
    Height = 13
    Caption = #24182#22312#19979#26041#36755#20837#26032#25968#25454#30340#20840#37096#20449#24687
  end
  object Edit1: TEdit
    Left = 150
    Top = 50
    Width = 100
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 150
    Top = 100
    Width = 100
    Height = 21
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 150
    Top = 150
    Width = 100
    Height = 21
    TabOrder = 2
  end
  object Edit4: TEdit
    Left = 150
    Top = 200
    Width = 100
    Height = 21
    TabOrder = 3
  end
  object DBGrid1: TDBGrid
    Left = 30
    Top = 250
    Width = 705
    Height = 250
    DataSource = Form3.DataSource1
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 360
    Top = 100
    Width = 75
    Height = 25
    Caption = #22686
    TabOrder = 5
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 480
    Top = 98
    Width = 75
    Height = 25
    Caption = #21024
    TabOrder = 6
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 362
    Top = 160
    Width = 75
    Height = 25
    Caption = #25913
    TabOrder = 7
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 480
    Top = 160
    Width = 75
    Height = 25
    Caption = #26597
    TabOrder = 8
    OnClick = Button4Click
  end
  object Edit5: TEdit
    Left = 256
    Top = 50
    Width = 100
    Height = 21
    TabOrder = 9
  end
  object Edit6: TEdit
    Left = 256
    Top = 100
    Width = 100
    Height = 21
    TabOrder = 10
  end
  object Edit7: TEdit
    Left = 256
    Top = 150
    Width = 100
    Height = 21
    TabOrder = 11
  end
  object Edit8: TEdit
    Left = 256
    Top = 200
    Width = 100
    Height = 21
    TabOrder = 12
  end
  object Button5: TButton
    Left = 637
    Top = 160
    Width = 100
    Height = 25
    Caption = #26597#35810#20840#37096#25968#25454
    TabOrder = 13
    OnClick = Button5Click
  end
end
