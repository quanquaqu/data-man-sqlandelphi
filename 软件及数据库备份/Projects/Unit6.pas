unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls;

type
  TForm6 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Edit5: TEdit;
    Edit6: TEdit;
    Label4: TLabel;
    Edit3: TEdit;
    Edit4: TEdit;
    DBGrid1: TDBGrid;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation
uses unit3;
{$R *.dfm}

procedure TForm6.Button1Click(Sender: TObject);
begin
form3.ADOQuery3.Close;
form3.ADOQuery3.SQL.Clear;
form3.ADOQuery3.SQL.Text:='insert into T3 values(:a,:b,:c)';
form3.ADOQuery3.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery3.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery3.Parameters.ParamByName('c').Value:=Edit3.Text;
form3.ADOQuery3.ExecSQL;
form3.ADOQuery3.Close;
form3.ADOQuery3.SQL.Clear;
form3.ADOQuery3.SQL.Add('select * from T3');
form3.ADOQuery3.Open;
end;

procedure TForm6.Button2Click(Sender: TObject);
begin
form3.ADOQuery3.Close;
form3.ADOQuery3.SQL.Clear;
form3.ADOQuery3.SQL.Text:='delete from T3 where 卫星ID=:a and 时间=:b';
form3.ADOQuery3.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery3.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery3.ExecSQL;
form3.ADOQuery3.Close;
form3.ADOQuery3.SQL.Clear;
form3.ADOQuery3.SQL.Add('select * from T3');
form3.ADOQuery3.Open;
end;

procedure TForm6.Button3Click(Sender: TObject);
begin
form3.ADOQuery3.Close;
form3.ADOQuery3.SQL.Clear;
form3.ADOQuery3.SQL.Text:='update T3 set 卫星ID=:c,时间=:d,对应地面站=:e where 卫星ID=:a and 时间=:b';
form3.ADOQuery3.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery3.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery3.Parameters.ParamByName('c').Value:=Edit4.Text;
form3.ADOQuery3.Parameters.ParamByName('d').Value:=Edit5.Text;
form3.ADOQuery3.Parameters.ParamByName('e').Value:=Edit6.Text;
form3.ADOQuery3.ExecSQL;
form3.ADOQuery3.Close;
form3.ADOQuery3.SQL.Clear;
form3.ADOQuery3.SQL.Add('select * from T3');
form3.ADOQuery3.Open;
end;

procedure TForm6.Button4Click(Sender: TObject);
begin
form3.ADOQuery3.Close;
form3.ADOQuery3.SQL.Clear;
form3.ADOQuery3.SQL.Add('select * from T3 where 卫星ID=:a and 时间=:b');
form3.ADOQuery3.Parameters.ParamByName('a').Value:=Edit1.Text;
form3.ADOQuery3.Parameters.ParamByName('b').Value:=Edit2.Text;
form3.ADOQuery3.Open;
end;

procedure TForm6.Button5Click(Sender: TObject);
begin
form3.ADOQuery3.Close;
form3.ADOQuery3.SQL.Clear;
form3.ADOQuery3.SQL.Add('select * from T3');
form3.ADOQuery3.Open;
end;

end.
